describe('Testing LimeSurvey - Herramienta Web', function() {

    let survey_title =  Cypress.env('survey_title').split('_').join(' ').split('pp').join(':') || "Title Test"

    beforeEach(function () {
        cy.viewport(1280, 1024)
        cy.visit('https://demo.limesurvey.org/index.php?r=admin/authentication/sa/login')
        cy.get('body').then(($body) => {
            if ($body.text().includes('Español - Spanish')) {
              cy.get('#loginlang').select('Español - Spanish', {force: true})
            } else {
              cy.get('#loginlang').select('English - English', {force: true})
            }
        })
        cy.get('.btn-default').contains('Log in').click({force: true})        
    })

    afterEach(function () {
        //Cerrar Sesión
        cy.get('.dropdown').contains(' demo ').click()
        cy.get('.dropdown').contains(' demo ').get('.dropdown-menu').contains('Logout').click()
    })

    it('1. Mostrar las encuestas disponibles', function() {
        //Listar las encuestas disponibles
        cy.wait(2000)
        cy.get('.selector__lstour--mainfunctionboxes').find('div[id="panel-2"]').contains('List available surveys').click()
        cy.screenshot(Cypress.env('version') + "/1_Encuestas_Disponibles") 
    })

    it('2. Crear una encuesta con una pregunta', function () {
        //Crear encuesta
        cy.contains('Create a new survey').click({force: true})
        cy.get('#surveyls_title').type(survey_title)
        cy.get('#select2-language-container').click()
        cy.get('#select2-language-results').contains('English').click()
        cy.contains('Save').click()
        cy.get('button[data-dismiss="alert"]').click({force: true})
        cy.contains('Structure').click()
        cy.contains('Add question group').click()
        cy.wait(2000)
        cy.get('#group_name_en').type(survey_title)
        cy.contains('Save and new group').click()
        cy.get('button[data-dismiss="alert"]').first().click({force: true})
        cy.get('button[data-dismiss="alert"]').click({force: true})
        cy.get('#group_name_en').type(survey_title+"2")
        cy.contains('Save').click()
        cy.get('button[data-dismiss="alert"]').click({force: true, multiple: true})
        cy.screenshot(Cypress.env('version') + "/2_Crear_Encuesta") 
    })

    it('3. Ver Temas', function(){
      cy.contains('Themes').first().click({force: true})
      cy.screenshot(Cypress.env('version') + "/3_Ver_Temas") 
    })
})
