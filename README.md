## Pruebas Automáticas 2018-2 - Universidad de Los Andes


### :person\_with\_blond\_hair: Integrantes 

|    **Nombre**     |           **Correo**              | 
| :---------------- |:---------------------------------:| 
| Brandon Bohorquez | ba.bohorquez10@uniandes.edu.co    |  
| Fabián Medina     | df.medinac@uniandes.edu.co        | 
| Fernando Reyes    | f.reyes948@uniandes.edu.co        | 

***

###  :computer: Aplicaciones

|  **Nombre**   |                  **URL**                      | **Tipo** |
| :------------ |:---------------------------------------------:| :-------:|
| Limesurvey    | https://github.com/LimeSurvey/LimeSurvey      | Web      |
| Omni Notes    | https://github.com/federicoiosue/Omni-Notes   | Móvil    |
| MyExpenses    | https://github.com/mtotschnig/MyExpenses      | Móvil    |



