﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Testing.Testing_config_data_types;
using Testing.Testing_config_data_types_web;

namespace Testing
{
    public class Aplication {

        public Aplication()
        {
            this.versiones = new List<string>();
        }

        public static string APLICATIONS_DIR = "Projects/";

        public static string PATH = MainWindow.DEFAULT_PATH + MainWindow.DEFAULT_DIRECTORY + APLICATIONS_DIR;

        public string nombre { get; set; }

        private string nombreArchivo { get {
                string nombreArch = nombre.ToLower();
                return Regex.Replace(nombreArch, @"\s+", "") + ".json";
            } }

        public string nombreDir
        {
            get
            {
                string nombreArch = nombre.ToLower();
                return String.Join("/", new string[] { Regex.Replace(nombreArch, @"\s+", "") });
            }
        }

        public string tipo { get; set; }

        public string url { get; set; }

        public string versionActiva;

        public List<string> versiones;

        /// <summary>
        /// Tipos de dato para aplicaciones móviles.
        /// </summary>
        public e2EDataType e2EDataType;

        public MonkeyDataType monkeyDataType;

        public BDTDataType bDTDataType;

        public MutationDataType MDataType;

        /// <summary>
        /// Tipos de dato para aplicaciones web.
        /// </summary>
        public e2EDataTypeWeb e2EDataTypeWeb;

        public MonkeyDataTypeWeb monkeyDataTypeWeb;

        public BDTDataTypeWeb bDTDataTypeWeb;

        /// <summary>
        /// Guarda la información de la aplicación en formato JSON en un archivo para la aplicación.
        /// </summary>
        public void GuardarApp()
        {
            //Sobreescribe el archivo si exite o lo crea si no.
            string jsonString = JsonConvert.SerializeObject(this);
            using (StreamWriter sw = File.CreateText(MainWindow.DEFAULT_PATH + MainWindow.DEFAULT_DIRECTORY+"\\Projects\\" + nombreArchivo))
            {
                sw.WriteLine(jsonString);
            }
        }

        /// <summary>
        /// Borra la aplicación y su carpeta de pruebas.
        /// </summary>
        public void borrarApp()
        {
            File.Delete(MainWindow.DEFAULT_PATH + MainWindow.DEFAULT_DIRECTORY + Aplication.APLICATIONS_DIR + nombreArchivo);
            string appdir = MainWindow.DEFAULT_PATH + MainWindow.DEFAULT_DIRECTORY + Aplication.APLICATIONS_DIR + EjecutarApp.APP_SAVES_DIR + nombreDir;
            if (Directory.Exists(appdir))
            {
                (new DirectoryInfo(appdir)).Delete(true);
            }
        }
    }
}
