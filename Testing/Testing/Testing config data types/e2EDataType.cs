﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testing.Testing_config_data_types
{
    public class e2EDataType
    {
        public string herramienta;

        public string navegador;

        public string resolucion;

        public bool? isHeadless;

        public string archivoPruebas;
    }
}
