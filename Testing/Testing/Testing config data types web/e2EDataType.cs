﻿namespace Testing.Testing_config_data_types_web
{
    public class e2EDataTypeWeb
    {
        public string herramienta;

        public string navegador;

        public string resolucion;

        public bool? isHeadless;

        public string archivoPruebas;

        public string genDataInfo;
    }
}
