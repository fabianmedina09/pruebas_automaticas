﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testing.Testing_config_data_types_web
{
    public class MonkeyDataTypeWeb
    {
        public string herramienta;

        public string navegador;

        public string resolucion;

        public bool? isHeadless;

        public string archivoPruebas;

        public int eventos;
    }
}
