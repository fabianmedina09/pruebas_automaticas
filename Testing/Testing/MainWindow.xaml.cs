﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Testing.Testing_config_data_types_web;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;

namespace Testing
{

    public partial class MainWindow : Window
    {
        public static string DEFAULT_PATH = "C:/";
        public static string DEFAULT_DIRECTORY = "UNIANDES/";
        string configPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "/config.txt");
        string errorComand = "no se reconoce como un comando interno o externo";
        private Aplication currentApp;

        public MainWindow(){
            InitializeComponent();
            verificarRequerimientos();
            loadConfigFile();
            crearDirectorio(DEFAULT_DIRECTORY + Aplication.APLICATIONS_DIR);
            CargarApps();
            progress.Value = 100;
            progressText.Text = "Listo!";
        }

        private void verificarRequerimientos() {
            if (!ejecutarComando("git --version", "").Contains(errorComand)){
                if (!Directory.Exists(System.IO.Path.Combine(DEFAULT_PATH, DEFAULT_DIRECTORY + "Projects/VisualRegression"))) {
                    ejecutarComando("git clone https://github.com/fabianmedina09/VisualRegression.git", DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects");
                }
            }

            if (ejecutarComando("ruby --version", "").Contains(errorComand)){
                rubyEstado.Text = "Instale Ruby";
                rubyEstado.Foreground = Brushes.Red;
            }else {
                if (ejecutarComando("gem --version", "").Contains(errorComand)) {
                    rubyEstado.Text = "Instale gem";
                    rubyEstado.Foreground = Brushes.Red;
                } else {
                    if (ejecutarComando("calabash-android version", "").Contains(errorComand)) {
                        rubyEstado.Text = "No instalado";
                        rubyEstado.Foreground = Brushes.Red;
                    }else {
                        if (ejecutarComando("adb --version", "").Contains(errorComand)){
                            rubyEstado.Text = "Android error";
                            rubyEstado.Foreground = Brushes.Red;
                        } else {
                            bool calabashExist = Directory.Exists(DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/CalabashFiles");
                            bool featuresExist = Directory.Exists(DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/CalabashFiles/features");
                            if (calabashExist == false || featuresExist == false) {
                                crearDirectorio(DEFAULT_DIRECTORY + "Projects/CalabashFiles");
                                ejecutarComando("calabash-android gen", DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/CalabashFiles");
                            }
                            if (featuresExist == false) {
                                rubyEstado.Text = "Features Error";
                                rubyEstado.Foreground = Brushes.Red;
                            } else {
                                rubyEstado.Text = "Instalado";
                                rubyEstado.Foreground = Brushes.Green;   
                            }
                        }
                    }
                }
            }
            
            if (ejecutarComando("node -v", "").Contains(errorComand)){
                nodeEstado.Text = "No Instalado";
                nodeEstado.Foreground = Brushes.Red;
            } else {
                nodeEstado.Text = "Instalado";
                nodeEstado.Foreground = Brushes.Green;
            }
            
            if (ejecutarComando("npm -v", "").Contains(errorComand)){
                npmEstado.Text = "No Instalado";
                npmEstado.Foreground = Brushes.Red;
            } else {
                npmEstado.Text = "Instalado";
                npmEstado.Foreground = Brushes.Green;
                crearDirectorio(DEFAULT_DIRECTORY + "Projects/CypressFiles");
                if (Directory.Exists(DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/CypressFiles/cypress") == false) {
                    ejecutarComando("npm install cypress --save-dev", DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/CypressFiles");
                    ejecutarComando(".\\node_modules\\.bin\\cypress open", DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/CypressFiles");
                }
                if (ejecutarComando(".\\node_modules\\.bin\\cypress version", DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/CypressFiles").Contains(errorComand)){
                    cypressEstado.Text = "No Instalado";
                    cypressEstado.Foreground = Brushes.Red;
                } else {
                    crearDirectorio(DEFAULT_DIRECTORY + "Projects/CypressFiles/resultados");
                    cypressEstado.Text = "Instalado";
                    cypressEstado.Foreground = Brushes.Green;
                }
            }
        }

        private void loadConfigFile() {
            if (File.Exists(configPath)) {
                using (StreamReader sr = new StreamReader(configPath)) {
                    DEFAULT_PATH = sr.ReadLine();
                }              
            }else {
                using (StreamWriter sw = new StreamWriter(configPath, true)) { 
                    sw.WriteLine(DEFAULT_PATH);
                }
            }
            pathDefault.Text = DEFAULT_PATH;
        }

        private void crearDirectorio(String path) {
            if (!Directory.Exists(System.IO.Path.Combine(DEFAULT_PATH, path))) {
                Directory.CreateDirectory(System.IO.Path.Combine(DEFAULT_PATH, path));
            }
        }

        public String ejecutarComando(String comando, String dir) {
            string path = dir.Equals("") ? "" : "cd /d " + dir+"&";

            ProcessStartInfo processStartInfo = new ProcessStartInfo("cmd", "/c "+ path+comando);
            processStartInfo.RedirectStandardOutput = true;
            processStartInfo.RedirectStandardError = true;
            processStartInfo.WindowStyle = ProcessWindowStyle.Maximized;  //No tiene efecto tal vez por ser la cmd
            processStartInfo.CreateNoWindow = true; //Ocultar consola de comandos
            processStartInfo.UseShellExecute = false;

            Process process = new Process();
            StringBuilder processOutput = new StringBuilder("");
            StringBuilder processErrorOutput = new StringBuilder("");
            process.OutputDataReceived += new DataReceivedEventHandler(
                (sendingProcess, outLine) =>
                {
                    // Collect the sort command output.
                    if (!String.IsNullOrEmpty(outLine.Data))
                    {
                        // Add the text to the collected output.
                        processOutput.AppendLine(outLine.Data);
                        Console.WriteLine(outLine.Data);
                    }
                }
            );
            process.ErrorDataReceived += new DataReceivedEventHandler(
                (sendingProcess, outLine) =>
                {
                    // Collect the sort command output.
                    if (!String.IsNullOrEmpty(outLine.Data))
                    {
                        // Add the text to the collected output.
                        processErrorOutput.AppendLine(outLine.Data);
                        Console.WriteLine(outLine.Data);
                    }
                }
            );
            process.StartInfo = processStartInfo;
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();
            process.Dispose();

            return processOutput.ToString() + " " + processErrorOutput.ToString();
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void registrarApp_Click(object sender, RoutedEventArgs e)
        {            
            RegistroApp regApp = new RegistroApp();
            regApp.ShowDialog();
            borrarApps();
            CargarApps();
        }

        private void cambiarDefaultPath_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog fdb = new System.Windows.Forms.FolderBrowserDialog();
            fdb.RootFolder = Environment.SpecialFolder.MyComputer;
            fdb.Description = "Selecione una ruta para su proyecto";
            fdb.ShowNewFolderButton = false;

            if (fdb.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                string tempDefaultPath = DEFAULT_PATH;
                DEFAULT_PATH = fdb.SelectedPath + "\\";
                crearDirectorio(DEFAULT_DIRECTORY);
                MoveFolder(tempDefaultPath + DEFAULT_DIRECTORY,  DEFAULT_PATH + DEFAULT_DIRECTORY);
                pathDefault.Text = DEFAULT_PATH;
                File.WriteAllText(configPath, DEFAULT_PATH);
            }
        }

        private void MoveFolder(string source, string destination){
            //move if directories are on the same volume
            if (System.IO.Path.GetPathRoot(source) == System.IO.Path.GetPathRoot(destination)) {
                Directory.Move(source, destination);
            }else{
                DirectoryInfo diSource = new DirectoryInfo(source);
                DirectoryInfo diTarget = new DirectoryInfo(destination);
                CopyFilesRecursively(diSource, diTarget);
                Directory.Delete(source, true);
            }
        }

        private void CopyFilesRecursively(DirectoryInfo source, DirectoryInfo target) {
            foreach (DirectoryInfo dir in source.GetDirectories())
                CopyFilesRecursively(dir, target.CreateSubdirectory(dir.Name));
            foreach (FileInfo file in source.GetFiles())
                file.CopyTo(System.IO.Path.Combine(target.FullName, file.Name));
        }

        private void configurarApp_Click(object sender, RoutedEventArgs e, Aplication app)
        {
            if (app.tipo == "Móvil")
            {
                Config config = new Config(app, this);
                config.ShowDialog();
            } else
            {
                WebConfig config = new WebConfig(app);
                config.ShowDialog();
            }
        }

        public String ejecutarCmdProgress(String comando, String dir, int value, string tipo)
        {
            this.Dispatcher.Invoke(() => {
                progressText.Text = "Iniciando Ejecución ...";
            });
            string path = dir.Equals("") ? "" : "cd /d " + dir + "&";
            ProcessStartInfo processStartInfo = new ProcessStartInfo("cmd", "/c " + path + comando);
            processStartInfo.RedirectStandardOutput = true;
            processStartInfo.RedirectStandardError = true;
            processStartInfo.WindowStyle = ProcessWindowStyle.Maximized;
            processStartInfo.CreateNoWindow = true;
            processStartInfo.UseShellExecute = false;
            Process process = new Process();
            StringBuilder processOutput = new StringBuilder("");
            StringBuilder processErrorOutput = new StringBuilder("");
            int contador = 0;

            process.OutputDataReceived += new DataReceivedEventHandler(
                (sendingProcess, outLine) => {
                    if (!String.IsNullOrEmpty(outLine.Data)) {
                        processOutput.AppendLine(outLine.Data);
                        this.Dispatcher.Invoke(() => { 
                            progress.Value = value;
                            if (value <= 79) value = value + 2;
                            int longCad = outLine.Data.Length > 75 ? 75 : outLine.Data.Length;
                            progressText.Text = outLine.Data.Substring(0, longCad) + "...";
                        });
                        contador++;
                        if (tipo == "RandomTesting" && contador >= 10) {
                            string comandoscrn = "adb exec-out screencap -p > " + currentApp.versionActiva + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + ".png";
                            ejecutarComando(comandoscrn, DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/" + tipo + "/" + currentApp.nombre + "/" + currentApp.versionActiva);
                            contador = 0;
                        }
                    }
                }
            );
            process.ErrorDataReceived += new DataReceivedEventHandler(
                (sendingProcess, outLine) => {
                    if (!String.IsNullOrEmpty(outLine.Data)){
                        processErrorOutput.AppendLine(outLine.Data);
                        this.Dispatcher.Invoke(() =>
                        {
                            progress.Value = value;
                            if (value <= 79) value = value + 2;
                            int longCad = outLine.Data.Length > 75 ? 75 : outLine.Data.Length;
                            progressText.Text = outLine.Data.Substring(0, longCad) + "...";
                        });
                        contador++;
                        if (tipo == "RandomTesting" && contador >= 10) {
                            string comandoscrn = "adb exec-out screencap -p > " + currentApp.versionActiva + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + ".png";
                            ejecutarComando(comandoscrn, DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/" + tipo + "/" + currentApp.nombre + "/" + currentApp.versionActiva);
                            contador = 0;
                        } else if (tipo == "CalabashFiles") {
                            string comandoscrn = "adb exec-out screencap -p > " + currentApp.versionActiva + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + ".png";
                            ejecutarComando(comandoscrn, DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/" + tipo + "/" + currentApp.nombre + "/" + currentApp.versionActiva + "/errors");
                        }
                    }
                }
            );
            process.StartInfo = processStartInfo;
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();
            string mensaje = tipo == "firmar" ? "APK Firmado" : "Finalizando ...";
            this.Dispatcher.Invoke(() => {
                progressText.Text = mensaje;
                progress.Value = 100;
            });
            process.Dispose();
            return processOutput.ToString() + " " + processErrorOutput.ToString();
        }

        private async void ejecutarApp_Click(object sender, RoutedEventArgs e, Aplication app){
            string comando = "";
            string resultado = "";
            string tipo_output = "";
            currentApp = app;
            progressText.Text = "Empezando Ejecución...";
            Thread.Sleep(1000);
            progressText.Text = "Ejecución en progreso...";
            progress.Value = 0;
            
            if (app.tipo == "Web" && app.e2EDataTypeWeb != null) {
                if (app.e2EDataTypeWeb.herramienta == "Cypress") {
                    String genDataArgs = "";
                    progressText.Text = "Ejecutando E2E con Cypress ...";
                    progress.Visibility = Visibility.Visible;
                    Thread.Sleep(1000);
                    string cbrowser = "";
                    if (app.e2EDataTypeWeb.navegador == "Electron")
                        cbrowser = "electron";
                    else
                        cbrowser = "chrome";
                    string cresolucion = "";
                    if (app.e2EDataTypeWeb.resolucion == "Default")
                        cresolucion = "viewportHeight=600,viewportWidth=900";
                    else {
                        char[] delimiters = { 'x', 'X' };
                        string[] resparts = app.e2EDataTypeWeb.resolucion.Split(delimiters);
                        cresolucion = "viewportHeight=" + resparts[0] + ",viewportWidth=" + resparts[1];
                    }

                    if (app.e2EDataTypeWeb.genDataInfo != "" && app.e2EDataTypeWeb.genDataInfo != null)
                    {
                        string json = "";
                        if (app.e2EDataTypeWeb.genDataInfo.Contains("https"))
                        {
                            json = getApiInfo(app.e2EDataTypeWeb.genDataInfo);

                        }
                        else
                        {
                            using (StreamReader r = new StreamReader("file.json"))
                            {
                                json = r.ReadToEnd();
                            }
                        }
                        dynamic allData = JsonConvert.DeserializeObject(json);
                        Random rnd = new Random();
                        dynamic elements = allData[rnd.Next(0, allData.Count)];
                        foreach (var element in elements)
                        {
                            string values = element.Value;
                            values = values.Replace(" ", "_");
                            values = values.Replace(",", "");
                            values = values.Replace(":", "pp");
                            genDataArgs = genDataArgs + "," + element.Name + "=\"" + values + "\"";
                        }
                    }

                    comando = ".\\node_modules\\.bin\\cypress run --spec "
                        + app.e2EDataTypeWeb.archivoPruebas + " --config video=false,reporter=json-stream," 
                        + cresolucion + " --browser " + cbrowser 
                        + ((bool) app.e2EDataTypeWeb.isHeadless? "": " --headed") 
                        + " --env " + "version=" + app.versionActiva + genDataArgs;
                    
                    await Task.Run(() =>
                    {                            
                        this.Dispatcher.Invoke(() => {
                            progress.Value = 10;
                        });
                        resultado = ejecutarCmdProgress(comando, DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/CypressFiles", 15, "");

                        //Ejecutar mock.
                        //resultado = File.ReadAllText(@"C:\UNIANDES\Projects\CypressFiles\cypressTestResults.txt");
                        tipo_output = "Cypress e2e";                        
                        Thread.Sleep(1000);
                    });
                }
            }
            else if (app.tipo == "Web" && app.monkeyDataTypeWeb != null)
            {
                if (app.monkeyDataTypeWeb.herramienta == "Cypress")
                {
                    progressText.Text = "Ejecutando random con Cypress ...";
                    progress.Visibility = Visibility.Visible;
                    Thread.Sleep(1000);
                    string cbrowser = "";
                    if (app.monkeyDataTypeWeb.navegador == "Electron")
                        cbrowser = "electron";
                    else
                        cbrowser = "chrome";
                    string cresolucion = "";
                    if (app.monkeyDataTypeWeb.resolucion == "Default")
                        cresolucion = "viewportHeight=600,viewportWidth=900";
                    else
                    {
                        char[] delimiters = { 'x', 'X' };
                        string[] resparts = app.monkeyDataTypeWeb.resolucion.Split(delimiters);
                        cresolucion = "viewportHeight=" + resparts[0] + ",viewportWidth=" + resparts[1];
                    }

                    comando = ".\\node_modules\\.bin\\cypress run --spec "
                        + app.monkeyDataTypeWeb.archivoPruebas + " --config video=false,reporter=json-stream,"
                        + cresolucion + " --browser " + cbrowser
                        + ((bool)app.monkeyDataTypeWeb.isHeadless ? "" : " --headed");

                    await Task.Run(() =>
                    {
                        this.Dispatcher.Invoke(() => {
                            progress.Value = 10;
                        });
                        resultado = ejecutarCmdProgress(comando, DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/CypressFiles", 15, "");
                        tipo_output = "Cypress random";
                        Thread.Sleep(1000);
                    });
                }
            }
            else if (app.tipo == "Web" && app.bDTDataTypeWeb != null)
            {
                if (app.bDTDataTypeWeb.herramienta == "Webdriver.io")
                {
                    modificarWdioConf(app.bDTDataTypeWeb);
                    comando = "node node_modules/webdriverio/bin/wdio wdio.conf.js";
                    //Ejecutar prueba.
                    await Task.Run(() =>
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            progress.Value = 10;
                        });
                        resultado = ejecutarCmdProgress(comando, DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/BDT", 15, "");
                        Thread.Sleep(1000);
                    });

                    //Ejecutar Mock.
                    //resultado = System.IO.File.ReadAllText(@"C:\UNIANDES\Projects\BDT\testResults.txt");

                    tipo_output = "Webdriver.io BDT";
                }
            }
            else if (app.tipo == "Móvil" && app.MDataType != null)
            {
                string resumen = "========== Resumen ==========\n";
                string[] lines = System.IO.File.ReadAllLines(DEFAULT_PATH + DEFAULT_DIRECTORY + Aplication.APLICATIONS_DIR
                    + @"mdroidplus-Mutation\mutants-source\" + app.MDataType.nombrePaquete + "-mutants.log");
                string[] mutatedFileLines;
                string[] originalFileLines;
                int iter = 0;
                foreach (string pApkPath in Directory.GetFiles(DEFAULT_PATH + DEFAULT_DIRECTORY + Aplication.APLICATIONS_DIR 
                    + @"mdroidplus-Mutation\mutants\"))
                {
                    if (iter < app.MDataType.numeroMutantes)
                    {
                        //Resultado ejecución.
                        string resultadoEjec = "";

                        //Detección mutante.
                        bool encontrado = false;

                        //Información mutante.
                        int numMutante = Int32.Parse(Path.GetFileNameWithoutExtension(pApkPath));
                        string mutantinfo = lines[numMutante - 1];
                        string mutatedFilePath = Path.GetFullPath((mutantinfo.Replace("Mutant " + numMutante + ": ", "").Split(new char[] { ';' }))[0]);
                        string mutatedFileBasePath = Path.GetFullPath(DEFAULT_PATH + DEFAULT_DIRECTORY + Aplication.APLICATIONS_DIR
                            + @"mdroidplus-Mutation\mutants-source\" + app.MDataType.nombrePaquete + "-mutant" + numMutante);
                        string originalFilePath = app.MDataType.directorioAMutar + mutatedFilePath.Replace(mutatedFileBasePath, "");
                        mutatedFileLines = System.IO.File.ReadAllLines(mutatedFilePath);
                        originalFileLines = System.IO.File.ReadAllLines(originalFilePath);
                        int mutatedLineNumber = int.Parse(Regex.Match(mutantinfo, @"in line (?<mutatedLineNumber>\d+)").Groups["mutatedLineNumber"].Value);

                        if (app.monkeyDataType != null && app.monkeyDataType.herramienta == "Monkey")
                        {
                            //Ejecutar pruebas.
                            comando = "adb uninstall " + app.url;
                            await Task.Run(() =>
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    progress.Value = 10;
                                });
                                ejecutarCmdProgress(comando, app.monkeyDataType.archivoPruebas + "/platform-tools", 15, "");
                                Thread.Sleep(1000);
                            });
                            comando = app.MDataType.programaParaFirmaApk + " sign --ks-pass pass:android --ks " + DEFAULT_PATH + DEFAULT_DIRECTORY
                                + Aplication.APLICATIONS_DIR + "mdroidplus-Mutation/debug.keystore " + pApkPath;
                            await Task.Run(() =>
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    progress.Value = 10;
                                });
                                ejecutarCmdProgress(comando, "", 15, "");
                                Thread.Sleep(1000);
                            });
                            comando = "adb install " + "\"" + pApkPath + "\"";
                            await Task.Run(() =>
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    progress.Value = 10;
                                });
                                ejecutarCmdProgress(comando, app.monkeyDataType.archivoPruebas + "/platform-tools", 15, "");
                                Thread.Sleep(1000);
                            });
                            comando = "adb shell monkey -p " + app.url + " -v " + app.monkeyDataType.eventos;
                            await Task.Run(() =>
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    progress.Value = 10;
                                });
                                resultadoEjec = ejecutarCmdProgress(comando, app.monkeyDataType.archivoPruebas + "/platform-tools", 15, "");
                                Thread.Sleep(1000);
                            });

                            //Ejecutar mock.
                            //resultadoEjec = System.IO.File.ReadAllText(@"C:\UNIANDES\Projects\mdroidplus-Mutation\adb-random-results.txt");


                            if (resultadoEjec.IndexOf("// Monkey finished") != -1)
                            {
                                encontrado = encontrado || false;
                            }
                            else
                            {
                                encontrado = encontrado || true;
                            }

                            tipo_output = "Mutation mobile monkey";

                            resultado += "\n" + resultadoEjec;
                        }
                        if (app.bDTDataType != null && app.bDTDataType.herramienta == "Calabash")
                        {
                            //Ejecutar pruebas.
                            comando = "calabash-android resign " + "\"" + pApkPath + "\"";
                            await Task.Run(() =>
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    progress.Value = 10;
                                });
                                ejecutarCmdProgress(comando, DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/CalabashFiles", 15, "");
                                Thread.Sleep(1000);
                            });
                            comando = "calabash-android run " + "\"" + pApkPath + "\"";
                            await Task.Run(() =>
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    progress.Value = 10;
                                });
                                resultadoEjec = ejecutarCmdProgress(comando, DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/CalabashFiles", 15, "");
                                Thread.Sleep(1000);
                            });

                            //Ejecutar mock.
                            //resultadoEjec = System.IO.File.ReadAllText(@"C:\UNIANDES\Projects\mdroidplus-Mutation\calabash-bdt-results.txt");

                            if (resultadoEjec.IndexOf("Failing Scenarios:") == -1)
                            {
                                encontrado = encontrado || false;
                            }
                            else
                            {
                                encontrado = encontrado || true;
                            }

                            resultado += "\n" + resultadoEjec;

                            tipo_output = "Mutation mobile calabash";
                        }

                        //Resumen ejecución.
                        if (!encontrado)
                        {
                            resumen += "Mutante no encontrado";
                        }
                        else
                        {
                            resumen += "Mutante encontrado";
                        }
                        resumen += ";" + mutantinfo;
                        resumen += ";" + mutatedFileLines[mutatedLineNumber - 1];
                        resumen += ";" + originalFileLines[mutatedLineNumber - 1];
                        resumen += "\n";

                        iter++;
                    }
                }
                resultado += "\n" + resumen;
                
                
            }
            else if (app.tipo == "Móvil" && app.monkeyDataType != null)
            {
                if (app.monkeyDataType.herramienta == "Monkey")
                {
                    progressText.Text = "Ejecutando Random con Monkey ...";
                    progress.Visibility = Visibility.Visible;
                    Thread.Sleep(1000);
                    crearDirectorio(DEFAULT_DIRECTORY + "Projects/RandomTesting/"+ app.nombre + "/" + app.versionActiva);

                    //Ejecutar prueba.
                    comando = "adb shell monkey -p " + app.url + " -v " + app.monkeyDataType.eventos + " -s";
                    await Task.Run(() =>
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            progress.Value = 10;
                        });
                        resultado = ejecutarCmdProgress(comando, "", 15, "RandomTesting");
                        Thread.Sleep(1000);
                    });
                    string comandoscrn = "adb exec-out screencap -p > " + app.versionActiva + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + ".png";
                    string path = DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/RandomTesting/" + app.nombre + "/" + app.versionActiva;
                    ejecutarComando(comandoscrn, path);

                    //Ejecutar mock.
                    //resultado = System.IO.File.ReadAllText(@"C:\UNIANDES\Projects\mdroidplus-Mutation\adb-random-results.txt");

                    tipo_output = "Monkey";
                }
            }
            else if (app.tipo == "Móvil" && app.bDTDataType != null)
            {
                if (app.bDTDataType.herramienta == "Calabash")
                {
                    progressText.Text = "Ejecutando BDT con Calabash ...";
                    progress.Visibility = Visibility.Visible;
                    Thread.Sleep(1000);
                    comando = "calabash-android resign " + app.url;
                    crearDirectorio(DEFAULT_DIRECTORY + "Projects/CalabashFiles/" + app.nombre + "/" + app.versionActiva);

                    //Ejecutar pruebas.
                    await Task.Run(() =>
                    {
                        this.Dispatcher.Invoke(() => {
                            progress.Value = 10;
                        });
                        ejecutarCmdProgress(comando, DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/CalabashFiles", 15, "firmar");
                        Thread.Sleep(1000);
                    });                    
                    comando = "calabash-android run " + app.url;
                    await Task.Run(() =>
                    {
                        this.Dispatcher.Invoke(() => {
                            progress.Value = 10;
                        });
                        resultado = ejecutarCmdProgress(comando, DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/CalabashFiles", 15, "CalabashFiles");
                        Thread.Sleep(1000);                       
                    });
                    string targetDir = DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/CalabashFiles/" + app.nombre + "/" + app.versionActiva;
                    string[] allFiles = System.IO.Directory.GetFiles(DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/CalabashFiles");
                    foreach (string file in allFiles) {
                        if (file.Contains("screenshot")){
                            System.IO.File.Move(file,
                            System.IO.Path.Combine(targetDir, System.IO.Path.GetFileName(file)));
                        }
                    }

                    //Ejecutar mock.
                    //resultado = System.IO.File.ReadAllText(@"C:\UNIANDES\Projects\mdroidplus-Mutation\calabash-bdt-results.txt");

                    tipo_output = "Calabash BDT";
                }                
            }

            progressText.Text = "Mostrando resultados";
            EjecutarApp exec = new EjecutarApp(app, comando, tipo_output, resultado);
            exec.ShowDialog();
            this.Dispatcher.Invoke(() => {
                progressText.Text = "Listo";
                progress.Value = 100;
            });
        }

        private string getApiInfo(string url) {
            string response = "";
            try
            {
                RestClient rclient = new RestClient();
                rclient.endPoint = url;
                response = string.Empty;
                response = rclient.makeRequest();
            }
            catch
            {
                response = "Error al consumir API";
            }
            return response;
        }

        private void modificarWdioConf(BDTDataTypeWeb bDTDataTypeWeb)
        {
            //Mover features
            string fileName = System.IO.Path.GetFileName(bDTDataTypeWeb.archivoFeatures);
            string targetPath = DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/BDT/features";

            // Use Path class to manipulate file and directory paths.
            string destFile = System.IO.Path.Combine(targetPath, fileName);

            // To copy a folder's contents to a new location:
            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }
            string[] files = System.IO.Directory.GetFiles(targetPath);
            foreach (string s in files)
            {
                System.IO.File.Delete(s);
            }            

            // To copy a file to another location and 
            // overwrite the destination file if it already exists.
            System.IO.File.Copy(bDTDataTypeWeb.archivoFeatures, destFile, true);

            //Mover steps
            fileName = System.IO.Path.GetFileName(bDTDataTypeWeb.archivoSteps);
            targetPath = DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/BDT/steps";

            // Use Path class to manipulate file and directory paths.
            destFile = System.IO.Path.Combine(targetPath, fileName);

            // To copy a folder's contents to a new location:
            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }
            files = System.IO.Directory.GetFiles(targetPath);
            foreach (string s in files)
            {
                System.IO.File.Delete(s);
            }

            // To copy a file to another location and 
            // overwrite the destination file if it already exists.
            System.IO.File.Copy(bDTDataTypeWeb.archivoSteps, destFile, true);

            //Modificar configuración de wdio.
            String currConfig = System.IO.File.ReadAllText(DEFAULT_PATH + DEFAULT_DIRECTORY + "Projects/BDT/" + "wdio.conf.js");

        }

        /// <summary>
        /// Carga las aplicaciones en el panel principal.
        /// </summary>
        private void CargarApps() {
            var fs = Directory.GetFiles(DEFAULT_PATH+DEFAULT_DIRECTORY+ Aplication.APLICATIONS_DIR);
            if (fs.Length > 0){
                int i = 0;
                int j = 0;
                foreach(string file in fs) {
                    if (file.Contains(".json")) {
                        Grid gr = new Grid();

                        Aplication app;
                        using (StreamReader sr = new StreamReader(file)) {
                            app = JsonConvert.DeserializeObject<Aplication>(sr.ReadToEnd());
                        }

                        StackPanel stkPan = new StackPanel();
                        //stkPan.VerticalAlignment = VerticalAlignment.Top;
                        //stkPan.Margin = new Thickness(0, 0, 0, 0);

                        TextBlock txtBl = new TextBlock();
                        txtBl.Text = app.nombre;
                        txtBl.TextAlignment = TextAlignment.Center;
                        //txtBl.Background = new SolidColorBrush(Color.FromRgb(0, 178, 161)); //TO-DO replace with material design.
                        txtBl.FontWeight = FontWeights.Bold;
                        txtBl.FontSize = 14;
                        stkPan.Children.Add(txtBl);

                        TextBlock txtBl2 = new TextBlock();
                        txtBl2.Text = app.tipo + " | " + app.versionActiva;
                        txtBl2.TextAlignment = TextAlignment.Center;
                        stkPan.Children.Add(txtBl2);

                        TextBlock txtBl3 = new TextBlock();
                        txtBl3.Text = app.url;
                        txtBl3.TextAlignment = TextAlignment.Center;
                        stkPan.Children.Add(txtBl3);

                        Button configurarAppBtn = new Button
                        {
                            Content = "Configurar"
                        };
                        configurarAppBtn.Click += delegate (object sender, RoutedEventArgs e) { configurarApp_Click(sender, e, app); };
                        stkPan.Children.Add(configurarAppBtn);

                        Button ejecutarAppBtn = new Button
                        {
                            Content = "Ejecutar"
                        };
                        ejecutarAppBtn.Click += delegate (object sender, RoutedEventArgs e) { ejecutarApp_Click(sender, e, app); };
                        stkPan.Children.Add(ejecutarAppBtn);

                        Button borrarAppBtn = new Button
                        {
                            Content = "Borrar"
                        };
                        borrarAppBtn.Click += delegate (object sender, RoutedEventArgs e) { borrarApp_Click(sender, e, app); };
                        stkPan.Children.Add(borrarAppBtn);

                        gr.SetValue(Grid.RowProperty, i);
                        gr.SetValue(Grid.ColumnProperty, j);
                        gr.Children.Add(stkPan);

                        AppView.Children.Add(gr);

                        if (j != 3)
                        {
                            j = j + 1;
                        }
                        else
                        {
                            i = i + 1;
                            j = 0;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Elimina las apps cargadas.
        /// </summary>
        private void borrarApps()
        {
            AppView.Children.Clear();
        }

        /// <summary>
        /// Borra la aplicación y su carpeta de pruebas.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="app"></param>
        private void borrarApp_Click(object sender, RoutedEventArgs e, Aplication app)
        {
            app.borrarApp();
            borrarApps();
            CargarApps();
        }

        private void visualReg_Click(object sender, RoutedEventArgs e)
        {
            VisualReg visual = new VisualReg();
            visual.ShowDialog();
        }

        private void genData_Click(object sender, RoutedEventArgs e)
        {
            GenData data = new GenData();
            data.ShowDialog();
        }
    }
}
