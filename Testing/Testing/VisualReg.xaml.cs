﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Testing
{


    public partial class VisualReg : Window
    {

        public VisualReg()
        {
            InitializeComponent();
            //dir /s/b archivos en una carpeta
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void visual_Click(object sender, RoutedEventArgs e)
        {
            crearDirectorio("C:/UNIANDES/Projects/VisualRegression/resultados");
            string folder1 = ejecutarComando("dir /b /a-d ", img1.Text);
            string folder2 = ejecutarComando("dir /b /a-d ", img2.Text);
            string[] files1 = folder1.Split(
                new[] { Environment.NewLine },
                StringSplitOptions.None
            );
            Array.Resize(ref files1, files1.Length - 1);
            string[] files2 = folder2.Split(
                new[] { Environment.NewLine },
                StringSplitOptions.None
            );
            Array.Resize(ref files2, files2.Length - 1);

            int match = 0;
            string resultado = "";
            string finalFolder = "vs_" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss");//img1.Text.Substring(0, img1.Text.Length - 2);
            if(files1.Length > 0 && files2.Length > 0)
                crearDirectorio("C:/UNIANDES/Projects/VisualRegression/resultados/"+finalFolder);
            for (int i = 0; i < files1.Length; i++) {
                for (int j = 0; j < files2.Length; j++)  {
                    if (files1[i].Equals(files2[j])) {
                        string comando = "node services/ResembleService.js "
                            + "\"" + img1.Text + "\\" + files1[i] + "\" "
                            + "\"" + img2.Text + "\\" + files2[j] + "\" " + finalFolder;
                       resultado = resultado +"\n" + "Comparando: " + files1[i] + "\n"
                            + ejecutarComando(comando,"C:/UNIANDES/Projects/VisualRegression");
                        match = match + 1;
                    }
                }
            }

            byte[] bytes = Encoding.Default.GetBytes(resultado);
            resultado = Encoding.UTF8.GetString(bytes);

            archivos.Text = "** Archivos ** \n Carpeta 1: " + files1.Length + " - Carpeta 2: " + files2.Length
                + "\n" + "Se encontraron " + match + " Coincidencias";
            resumen.Text = resultado + "\n Las imagenes de comparación de encuentran disponibles en: \n "
                + "C:/UNIANDES/Projects/VisualRegression/resultados/" + finalFolder;
        }

        private void regresar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private String ejecutarComando(String comando, String dir)
        {
            string path = dir.Equals("") ? "" : "cd /d " + dir + "&";

            ProcessStartInfo processStartInfo = new ProcessStartInfo("cmd", "/c " + path + comando);
            processStartInfo.RedirectStandardOutput = true;
            processStartInfo.RedirectStandardError = true;
            processStartInfo.WindowStyle = ProcessWindowStyle.Maximized;  //No tiene efecto tal vez por ser la cmd
            processStartInfo.CreateNoWindow = true; //Ocultar consola de comandos
            processStartInfo.UseShellExecute = false;

            Process process = new Process();
            StringBuilder processOutput = new StringBuilder("");
            StringBuilder processErrorOutput = new StringBuilder("");
            process.OutputDataReceived += new DataReceivedEventHandler(
                (sendingProcess, outLine) =>
                {
                    // Collect the sort command output.
                    if (!String.IsNullOrEmpty(outLine.Data))
                    {
                        // Add the text to the collected output.
                        processOutput.AppendLine(outLine.Data);
                        Console.WriteLine(outLine.Data);
                    }
                }
            );
            process.ErrorDataReceived += new DataReceivedEventHandler(
                (sendingProcess, outLine) =>
                {
                    // Collect the sort command output.
                    if (!String.IsNullOrEmpty(outLine.Data))
                    {
                        // Add the text to the collected output.
                        processErrorOutput.AppendLine(outLine.Data);
                        Console.WriteLine(outLine.Data);
                    }
                }
            );

            process.StartInfo = processStartInfo;
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();
            process.Dispose();

            return processOutput.ToString() + " " + processErrorOutput.ToString();
        }

        private void crearDirectorio(String path)
        {
            if (!Directory.Exists(System.IO.Path.Combine(path)))
            {
                Directory.CreateDirectory(System.IO.Path.Combine(path));
            }
        }

    }
}
