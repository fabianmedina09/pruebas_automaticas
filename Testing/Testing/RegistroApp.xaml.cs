﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Testing
{
    /// <summary>
    /// Lógica de interacción para RegistroApp.xaml
    /// </summary>
    public partial class RegistroApp : Window
    {
        public RegistroApp()
        {
            InitializeComponent();
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void tipo_ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            URLPanel.Visibility = Visibility.Visible;
            switch (tipoComboBox.SelectedIndex) {
                case 0:
                    urlLabel.Text = "URL";
                    break;
                case 1:
                    urlLabel.Text = "Paquete";
                    break;
            }
        }

        private void registrarApp_Click(object sender, RoutedEventArgs e)
        {
            int select = tipoComboBox.SelectedIndex;

            if (nombreApp.Text != "" && urlText.Text != "")
            {
                //Crear Json Local
                Aplication newApp = new Aplication();
                newApp.nombre = nombreApp.Text;
                newApp.tipo = (string)((ComboBoxItem) tipoComboBox.SelectedItem).Content ;
                newApp.url = urlText.Text;
                newApp.versiones.Add(versionText.Text);
                newApp.versionActiva = versionText.Text;
                newApp.GuardarApp();

                this.Close();
            }
            else {
                errorTxt.Text = "Campos Inválidos o tipo de prueba no disponible.";
            }

        }
    }


}
