﻿using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Testing.Testing_config_data_types_web;

namespace Testing
{
    /// <summary>
    /// Lógica de interacción para RegistroApp.xaml
    /// </summary>
    public partial class WebConfig : Window
    {
        private Aplication app;

        public WebConfig()
        {
            InitializeComponent();
        }

        public WebConfig(Aplication appParam) : this()
        {
            this.app = appParam;
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Muestra el panel de configuración de la prueba seleccionada.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tipo_ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (tipoComboBox.SelectedIndex)
            {
                case 0: //E2E
                    E2EPaneWeb.Visibility = Visibility.Visible;
                    MonkeyPane.Visibility = Visibility.Collapsed;
                    BDTPaneWeb.Visibility = Visibility.Collapsed;
                    VersionPaneWeb.Visibility = Visibility.Collapsed;
                    break;
                case 1: //Monkey
                    E2EPaneWeb.Visibility = Visibility.Collapsed;
                    MonkeyPane.Visibility = Visibility.Visible;
                    BDTPaneWeb.Visibility = Visibility.Collapsed;
                    VersionPaneWeb.Visibility = Visibility.Collapsed;
                    break;
                case 2: //BDT
                    E2EPaneWeb.Visibility = Visibility.Collapsed;
                    MonkeyPane.Visibility = Visibility.Collapsed;
                    BDTPaneWeb.Visibility = Visibility.Visible;
                    VersionPaneWeb.Visibility = Visibility.Collapsed;
                    break;
                case 3: //Version
                    E2EPaneWeb.Visibility = Visibility.Collapsed;
                    MonkeyPane.Visibility = Visibility.Collapsed;
                    BDTPaneWeb.Visibility = Visibility.Collapsed;
                    VersionPaneWeb.Visibility = Visibility.Visible;
                    cargarVersiones();
                    break;
            }
        }

        /// <summary>
        /// Configura la prueba de la aplicación. Se llama al presionar el botón "Finalizar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void configApp_Click(object sender, RoutedEventArgs e)
        {
            int select = tipoComboBox.SelectedIndex;

            switch (tipoComboBox.SelectedIndex)
            {
                case 0: //E2E
                    app.e2EDataTypeWeb = new e2EDataTypeWeb
                    {
                        herramienta = herramientasComboBox.Text,
                        navegador = navegadorComboBox.Text,
                        resolucion = resolucionComboBox.Text,
                        isHeadless = isHeadless.IsChecked,
                        archivoPruebas = fileLocationCypress.Text,
                        genDataInfo = genDataCypress.Text
                    };
                    break;
                case 1: //Monkey
                    app.monkeyDataTypeWeb = new MonkeyDataTypeWeb
                    {
                        herramienta = herramientasComboBoxMonkey.Text,
                        navegador = navegadorComboBoxMonkey.Text,
                        resolucion = resolucionComboBoxMonkey.Text,
                        isHeadless = isHeadlessMonkey.IsChecked,
                        archivoPruebas = fileLocationCypressMonkey.Text,
                        eventos = Int32.Parse(numEventos.Text)
                    };
                    break;
                case 2: //BDT
                    app.bDTDataTypeWeb = new BDTDataTypeWeb
                    {
                        herramienta = herramientasComboBoxBDTWeb.Text,
                        navegador = navegadorComboBoxBDTWeb.Text,
                        resolucion = resolucionComboBoxBDTWeb.Text,
                        archivoFeatures = featuresLocationBDTWeb.Text,
                        archivoSteps = stepsLocationBDTWeb.Text
                    };
                    break;
                case 3: //Version
                    break;
                default:
                    MessageBox.Show("Seleccione un tipo de prueba.");
                    break;
            }

            app.GuardarApp();
            this.Close();
        }

        private void herramientasComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void navegadorComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void resolucionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        /// <summary>
        /// Define la ubicación del archivo de pruebas E2E de Cypress.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CambiarButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            //openFileDialog.InitialDirectory = Aplication.PATH;
            if (openFileDialog.ShowDialog() == true)
                fileLocationCypress.Text = openFileDialog.FileName;
        }

        /// <summary>
        /// Cambia la ubicación del spec de Cypress para e2e web.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CambiarDirButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog fdb = new System.Windows.Forms.FolderBrowserDialog
            {
                RootFolder = Environment.SpecialFolder.MyComputer,
                Description = "Selecione la ruta del spec",
                ShowNewFolderButton = false
            };

            if (fdb.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                fileLocationCypress.Text = fdb.SelectedPath;
            }
        }       

        /// <summary>
        /// Cambia la ubicación del spec de Cypress para random web.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CambiarDirButtonMonkey_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog fdb = new System.Windows.Forms.FolderBrowserDialog
            {
                RootFolder = Environment.SpecialFolder.MyComputer,
                Description = "Selecione la ruta del spec",
                ShowNewFolderButton = false
            };

            if (fdb.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                fileLocationCypressMonkey.Text = fdb.SelectedPath;
            }
        }

        /// <summary>
        /// Define la ubicación del archivo de features BDT web.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CambiarFeaturesLocationBDTWebButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = Aplication.PATH
            };
            if (openFileDialog.ShowDialog() == true)
                featuresLocationBDTWeb.Text = openFileDialog.FileName;
        }

        /// <summary>
        /// Cambia la ubicación de los steps para BDT web.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CambiarStepsLocationBDTWebButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = Aplication.PATH
            };
            if (openFileDialog.ShowDialog() == true)
                featuresLocationBDTWeb.Text = openFileDialog.FileName;
        }

        /// <summary>
        /// Cambia la versión activa de la aplicación.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void activarVersion(object sender, RoutedEventArgs e)
        {
            app.versionActiva = (string) activarVersionComboBoxWeb.SelectedValue;
            activeVersionTextBox.Text = app.versionActiva;
        }

        /// <summary>
        /// Agrega una versión a la aplicación.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void agregarVersion(object sender, RoutedEventArgs e)
        {
            app.versiones.Add(newVersionTextBoxWeb.Text);
            cargarVersiones();
        }

        /// <summary>
        /// Carga la lista de versiones disponibles de la aplicación.
        /// </summary>
        public void cargarVersiones()
        {
            activarVersionComboBoxWeb.Items.Clear();
            foreach (string ver in app.versiones)
            {
                activarVersionComboBoxWeb.Items.Add(ver);
            }
            activeVersionTextBox.Text = app.versionActiva;
        }
    }


}
