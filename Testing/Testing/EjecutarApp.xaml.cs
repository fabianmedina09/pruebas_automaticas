﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Testing
{
    /// <summary>
    /// Lógica de interacción para RegistroApp.xaml
    /// </summary>
    public partial class EjecutarApp : Window
    {
        private Aplication app;
        private string comando;
        private string resultado;
        private StringBuilder cuerpoHtml;
        private MainWindow m = new MainWindow();

        public static string APP_SAVES_DIR = "app_saves/";

        public EjecutarApp()
        {
            InitializeComponent();
        }

        public EjecutarApp(Aplication app, string exec, string comando, string result)
        {
            InitializeComponent();
            this.app = app;
            this.comando = comando;

            byte[] bytes = Encoding.Default.GetBytes(result);
            this.resultado = Encoding.UTF8.GetString(bytes);

            resultTest.Children.Add(new TextBlock { Text = "Información de la aplicación", TextAlignment = TextAlignment.Center, FontSize = 14, FontWeight = FontWeights.Bold });
            agregarElementoReporteHTML("h3", "Información de la aplicación");
            resultTest.Children.Add(new TextBlock { Text = "Nombre: " + app.nombre + " | Plataforma: " + app.tipo + " | Versión: " + app.versionActiva + "\n", FontSize = 12, TextAlignment = TextAlignment.Center });
            agregarElementoReporteHTML("p", "Nombre: " + app.nombre + " | Plataforma: " + app.tipo + " | Versión: " + app.versionActiva);
            resultTest.Children.Add(new TextBlock { Text = "Información de la prueba", TextAlignment = TextAlignment.Center, FontSize = 14, FontWeight = FontWeights.Bold });
            agregarElementoReporteHTML("h3", "Información de la prueba");
            agregarElementoReporteHTML("p", "Comando: " + exec);

            if (comando == "Cypress e2e") {
                string detail = "Herramienta: " + app.e2EDataTypeWeb.herramienta + " | Navegador: " + app.e2EDataTypeWeb.navegador + " | Resolución: " + app.e2EDataTypeWeb.resolucion + "\n"
                    + "Headless: " + app.e2EDataTypeWeb.isHeadless
                    + " | Archivo: " + app.e2EDataTypeWeb.archivoPruebas.Replace("C:\\UNIANDES\\Projects\\CypressFiles\\cypress\\integration\\", "");
                if (app.e2EDataTypeWeb.genDataInfo != "") {
                    detail = detail + "\nMockaroo Data: \n" + app.e2EDataTypeWeb.genDataInfo;
                }
                resultTest.Children.Add(new TextBlock { Text = detail, FontSize = 12, TextAlignment = TextAlignment.Center });
                agregarElementoReporteHTML("p", detail);
                ProcesarResultado(resultado);
            } else if (comando == "Cypress random") {
                string detail = "Herramienta: " + app.monkeyDataTypeWeb.herramienta + " | Navegador: " + app.monkeyDataTypeWeb.navegador + " | Resolución: " + app.monkeyDataTypeWeb.resolucion + "\n"
                    + "Headless: " + app.monkeyDataTypeWeb.isHeadless
                    + " | Archivo: " + app.monkeyDataTypeWeb.archivoPruebas.Replace("C:\\UNIANDES\\Projects\\CypressFiles\\cypress\\integration\\", "");
                resultTest.Children.Add(new TextBlock { Text = detail, FontSize = 12, TextAlignment = TextAlignment.Center });
                agregarElementoReporteHTML("p", detail);
                ProcesarResultado(resultado);
            }
            else if (comando == "Webdriver.io BDT") {
                ProcesarResultadoWdioBDT(resultado);
            } else if (comando == "Mutation mobile monkey" || comando == "Mutation mobile calabash") {
                ProcesarResultadoMutationMobileRandom(resultado);
            } else if (comando == "Monkey") {
                string fabricante = m.ejecutarComando("adb shell getprop ro.product.vendor.manufacturer", "").Replace("\n", ""); ;
                string sdk = m.ejecutarComando("adb shell getprop ro.build.version.sdk", "").Replace("\n", ""); ;
                string detail = "# Eventos: " + app.monkeyDataType.eventos + " | Semilla: Si\n Fabricante: "+fabricante+" | SDK: " + sdk;
                resultTest.Children.Add(new TextBlock { Text = detail, FontSize = 12, TextAlignment = TextAlignment.Center});
                agregarElementoReporteHTML("p", detail);
                ProcesarResultadoAdb();
            } else if (comando == "Calabash BDT") {                
                string fabricante = m.ejecutarComando("adb shell getprop ro.product.vendor.manufacturer", "").Replace("\n","");
                string sdk = m.ejecutarComando("adb shell getprop ro.build.version.sdk", "").Replace("\n", ""); ;
                string detail = " Fabricante: " +fabricante+ " | SDK: " + sdk;
                resultTest.Children.Add(new TextBlock { Text = detail, FontSize = 12, TextAlignment = TextAlignment.Center });
                agregarElementoReporteHTML("p", detail);
                ProcesarResultadoCalabash();
            }else {
                panelResultados.Children.Add(new TextBlock { Text = resultado, Margin = new Thickness(Left = 5) });
                agregarElementoReporteHTML("p", resultado);
            }
           
        }

        /// <summary>
        /// Procesa el resultado de ejecutar BDT con calabash.
        /// </summary>
        private void ProcesarResultadoCalabash()
        {
            string[] reporte = resultado.Split(new string[] { Environment.NewLine },StringSplitOptions.RemoveEmptyEntries);
            string linea = "";

            //Información BDT.
            string feature;
            string scenario;
            string error;
            string ubicacionError;

            //Expresiones regulares.
            Regex rx = new Regex(@"Feature:");
            Regex rxSce = new Regex(@"Scenario:");
            Regex rxStep = new Regex(@"^\s*(And|Given|When|Then)");
            Regex rxRes = new Regex(@"^\d+ scenario");
            Regex rxFail = new Regex(@"Failing Scenarios:");
            Regex rxCom = new Regex(@"^\s*#\s*");

            for (int i = 0; i < reporte.Length; i++)
            {
                linea = reporte[i];
                if (rx.IsMatch(linea))
                {
                    feature = linea;
                    panelResultados.Children.Add(new TextBlock { Text = feature, Background = new SolidColorBrush(Colors.LightGreen) });
                    agregarElementoReporteHTML("p", feature);
                    i++;
                    bool termino = false;
                    while (!termino)
                    {
                        linea = reporte[i];
                        if (rxRes.IsMatch(linea) || rxFail.IsMatch(linea) || rx.IsMatch(linea))
                        {
                            termino = true;
                            i--;
                        }
                        else if (rxSce.IsMatch(linea))
                        {
                            scenario = linea;
                            panelResultados.Children.Add(new TextBlock { Text = scenario, Background = new SolidColorBrush(Colors.LightGreen) });
                            agregarElementoReporteHTML("p", scenario);
                            i++;
                        }
                        else if ( linea == Environment.NewLine || linea == "")
                        {
                            i++;
                        }
                        else if(rxStep.IsMatch(linea))
                        {
                            panelResultados.Children.Add(new TextBlock { Text = linea.Trim(), Margin = new Thickness(Left = 5), FontWeight = FontWeights.Bold });
                            agregarElementoReporteHTML("p", linea.Trim());
                            i++;
                        }
                        else if (rxCom.IsMatch(linea))
                        {
                            i++;
                        }
                        else
                        {
                            error = linea;
                            ubicacionError = reporte[i + 1];
                            panelResultados.Children.Add(new TextBlock { Text = error, Background = new SolidColorBrush(Colors.Red), Margin = new Thickness(Left = 5), FontWeight = FontWeights.Bold });
                            agregarElementoReporteHTML("p", "<b>" + error + "</b>");
                            panelResultados.Children.Add(new TextBlock { Text = ubicacionError, Background = new SolidColorBrush(Colors.Red), Margin = new Thickness(Left = 5), FontWeight = FontWeights.Bold });
                            agregarElementoReporteHTML("p", ubicacionError);
                            i += 2;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Procesa el resultado de ejecutar random testing con el monkey del ADB.
        /// </summary>
        private void ProcesarResultadoAdb()
        {
            string[] reporte = resultado.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            string linea = "";

            //Información RT.
            long seed = 0;
            int count = 0;
            int inyectados = 0;
            string error = null;

            //Expresiones regulares.
            Regex rxInfo = new Regex(@":Monkey: seed=(?<seed>\d+) count=(?<count>\d+)");
            Regex rxInyectados = new Regex(@"Events injected: (?<injected>\d+)");
            Regex rxError = new Regex(@"// Long Msg:");
            GroupCollection rxGroups;

            for (int i = 0; i < reporte.Length; i++)
            {
                linea = reporte[i];
                if (rxInfo.IsMatch(linea))
                {
                    rxGroups = rxInfo.Match(linea).Groups;
                    seed = long.Parse(rxGroups["seed"].Value);
                    count = int.Parse(rxGroups["count"].Value);
                }
                else if (rxInyectados.IsMatch(linea))
                {
                    rxGroups = rxInyectados.Match(linea).Groups;
                    inyectados = int.Parse(rxGroups["injected"].Value);
                }
                else if (rxError.IsMatch(linea))
                {
                    error = linea.Replace(@"// Long Msg:", "");
                }
            }
            panelResultados.Children.Add(new TextBlock { Text = "RT - adb monkey", Background = new SolidColorBrush(Colors.LightGreen) });
            agregarElementoReporteHTML("h5", "RT - adb monkey");
            panelResultados.Children.Add(new TextBlock { Text = "Semilla: " + seed, Margin = new Thickness(Left = 5) });
            agregarElementoReporteHTML("h5", "Semilla: " + seed);
            panelResultados.Children.Add(new TextBlock { Text = "Eventos a inyectar: " + count, Margin = new Thickness(Left = 5) });
            agregarElementoReporteHTML("h5", "Eventos a inyectar: " + count);
            panelResultados.Children.Add(new TextBlock { Text = "Eventos inyectados: " + inyectados, Margin = new Thickness(Left = 5) });
            agregarElementoReporteHTML("h5", "Eventos inyectados: " + inyectados);
            if (!(error is null))
            {
                agregarElementoReporteHTML("h5", "Error: ");
                agregarElementoReporteHTML("p", error);
            }
            
        }

        /// <summary>
        /// Procesa el resultado de la ejecución de mutation testing en móviles.
        /// </summary>
        /// <param name="resultado"></param>
        private void ProcesarResultadoMutationMobileRandom(string resultado)
        {
            TextBlock detailsTextBlock = new TextBlock { FontSize = 12, TextAlignment = TextAlignment.Center };
            resultTest.Children.Add(detailsTextBlock);

            string[] lines = resultado.Split(new char[] { '\n'}, StringSplitOptions.None);
            bool isResumen = false;
            bool isResult = false;
            double encontrados = 0;
            double noEncontrados = 0;
            foreach(string pline in lines)
            {
                if (pline == "========== Resumen ==========")
                    isResumen = true;
                if (isResumen)
                {
                    if (isResult && pline != "")
                    {
                        string[] resDetails = pline.Split(new char[] { ';' });
                        panelResultados.Children.Add(new TextBlock { Text = resDetails[1]
                            , Background = new SolidColorBrush(Colors.Orange), Margin = new Thickness(Left = 5),FontWeight = FontWeights.Bold
                        });
                        agregarElementoReporteHTML("h3", resDetails[1]);
                        switch (resDetails[0])
                        {
                            case "Mutante no encontrado":
                                panelResultados.Children.Add(new TextBlock { Text = resDetails[0]
                                    , Background = new SolidColorBrush(Colors.Red),
                                    Margin = new Thickness(Left = 5),
                                    FontWeight = FontWeights.Bold
                                }); //TO-DO replace with material design.});
                                noEncontrados++;
                                break;
                            case "Mutante encontrado":
                                panelResultados.Children.Add(new TextBlock { Text = resDetails[0]
                                    , Background = new SolidColorBrush(Colors.LightGreen),
                                    Margin = new Thickness(Left = 5),
                                    FontWeight = FontWeights.Bold
                                });
                                encontrados++;
                                break;
                        }
                        agregarElementoReporteHTML("h5", resDetails[0]);
                        panelResultados.Children.Add(new TextBlock
                        {
                            Text = "Mutacion: " + resDetails[2],
                            Margin = new Thickness(Left = 5)
                        });
                        agregarElementoReporteHTML("h5", "Mutacion: ");
                        agregarElementoReporteHTML("p",resDetails[2]);
                        panelResultados.Children.Add(new TextBlock
                        {
                            Text = "Línea mutada: " + resDetails[3],
                            Margin = new Thickness(Left = 5)
                        });
                        agregarElementoReporteHTML("h5", "Línea mutada: ");
                        agregarElementoReporteHTML("p", WebUtility.HtmlEncode(resDetails[3]));
                        panelResultados.Children.Add(new TextBlock
                        {
                            Text = "Línea original: " + resDetails[4],
                            Margin = new Thickness(Left = 5)
                        });
                        agregarElementoReporteHTML("h5", "Línea original: ");
                        agregarElementoReporteHTML("p", WebUtility.HtmlEncode(resDetails[4]));
                    }
                    else
                        isResult = true;
                }
            }

            string detail = "Número mutantes: " + app.MDataType.numeroMutantes + " | Paquete: " + app.MDataType.nombrePaquete 
                + " \n| Densidad de defectos: " + (encontrados/(encontrados+noEncontrados)*100) + "%";
            detailsTextBlock.Text = detail;
            agregarElementoReporteHTML("h1", "Resúmen");
            agregarElementoReporteHTML("p", detail);
        }

        /// <summary>
        /// Procesa la salida de ejecución de Webdriver.io BDT para web.
        /// </summary>
        /// <param name="resultado"></param>
        private void ProcesarResultadoWdioBDT(string resultado)
        {
            JObject wdioLogJson = JObject.Parse(resultado);
            string detail = "Herramienta: " + app.bDTDataTypeWeb.herramienta + " | Navegador: " + app.bDTDataTypeWeb.navegador + " | Resolución: " + app.bDTDataTypeWeb.resolucion + "\n"
                + " | Features: " + app.bDTDataTypeWeb.archivoFeatures.Replace("C:\\UNIANDES\\Projects\\BDT\\", "")
                + " | Steps: " + app.bDTDataTypeWeb.archivoSteps.Replace("C:\\UNIANDES\\Projects\\BDT\\", "");
            resultTest.Children.Add(new TextBlock { Text = detail, FontSize = 12, TextAlignment = TextAlignment.Center });
            agregarElementoReporteHTML("p", detail);
            foreach (JObject suite in wdioLogJson["suites"])
            {
                JArray tests = (JArray) suite["tests"];
                if (tests.ToArray().Length == 0)
                {
                    panelResultados.Children.Add(new TextBlock { Text = "Feature: " + suite["name"], Background = new SolidColorBrush(Colors.LightGreen) });
                    agregarElementoReporteHTML("h3", "Feature: ");
                    agregarElementoReporteHTML("p", (string) suite["name"]);

                }
                else
                {
                    panelResultados.Children.Add(new TextBlock { Text = "Scenario: " + suite["name"], Background = new SolidColorBrush(Colors.LightGreen) });
                    agregarElementoReporteHTML("h3", "Scenario: ");
                    agregarElementoReporteHTML("p", (string)suite["name"]);
                    foreach (JObject test in tests)
                    {
                        String testResult = (String)test["state"];
                        switch (testResult)
                        {
                            case "pass":
                                panelResultados.Children.Add(new TextBlock { Text = "Paso exitoso", Background = new SolidColorBrush(Colors.LightGreen), Margin = new Thickness(Left = 5), FontWeight = FontWeights.Bold });
                                agregarElementoReporteHTML("h5", "Paso exitoso");
                                panelResultados.Children.Add(new TextBlock { Text = (string)test["name"], Margin = new Thickness(Left = 5) });
                                agregarElementoReporteHTML("p", (string)test["name"]);
                                break;
                            case "fail":
                                panelResultados.Children.Add(new TextBlock { Text = "Prueba fallida", Background = new SolidColorBrush(Colors.Red), Margin = new Thickness(Left = 5), FontWeight = FontWeights.Bold });
                                agregarElementoReporteHTML("h5", "Prueba fallida");
                                panelResultados.Children.Add(new TextBlock { Text = (string)test["name"], Margin = new Thickness(Left = 5) });
                                agregarElementoReporteHTML("p", (string)test["name"]);
                                panelResultados.Children.Add(new TextBlock { Text = "Error: " + (string)test["error"], Margin = new Thickness(Left = 5) });
                                agregarElementoReporteHTML("p", "<b>Error: </b>" + (string)test["error"]);
                                break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Procesa el resultado de ejcutar Cypress para e2e o random.
        /// </summary>
        /// <param name="resultado"></param>
        private void ProcesarResultado(string resultado)
        {
            Regex rx = new Regex(@"\[""([a-z]+)"",(.+)\]");

            MatchCollection lines = rx.Matches(resultado);

            string cyLogType;
            JObject cyLogJson;
            foreach (Match line in lines)
            {
                cyLogType = line.Groups[1].Value;
                switch (cyLogType)
                {
                    case "pass":
                        panelResultados.Children.Add(new TextBlock { Text = "Prueba exitosa", Background = new SolidColorBrush(Colors.LightGreen),FontWeight = FontWeights.Bold, Margin = new Thickness(Left = 5) }); //TO-DO replace with material design.});
                        agregarElementoReporteHTML("h2", "Prueba exitosa");
                        cyLogJson = JObject.Parse(line.Groups[2].Value);
                        panelResultados.Children.Add(new TextBlock { Text = (string) cyLogJson["fullTitle"], Margin = new Thickness(Left = 5) });
                        agregarElementoReporteHTML("p", (string)cyLogJson["fullTitle"]);
                        break;
                    case "fail":
                        panelResultados.Children.Add(new TextBlock { Text = "Prueba fallida", Background = new SolidColorBrush(Colors.Red), FontWeight = FontWeights.Bold,Margin = new Thickness(Left = 5) });
                        agregarElementoReporteHTML("h2", "Prueba fallida");
                        cyLogJson = JObject.Parse(line.Groups[2].Value);
                        panelResultados.Children.Add(new TextBlock { Text = (string)cyLogJson["fullTitle"], Margin = new Thickness(Left = 5)});
                        agregarElementoReporteHTML("p", (string)cyLogJson["fullTitle"]);
                        panelResultados.Children.Add(new TextBlock { Text = (string)cyLogJson["err"], Margin = new Thickness(Left = 5) });
                        agregarElementoReporteHTML("p", (string)cyLogJson["err"]);
                        break;
                    case "start": 
                        break;
                    case "end":
                        break;
                }
            }
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void regresar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            
        }

        /// <summary>
        /// Guarda el resultado en un archivo de texto en la carpeta correspondiente.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void save_Click(object sender, RoutedEventArgs e)
        {
            //Guardar resultado de ejecución de prueba.
            string tipoPrueba = "";
            if (comando == "Cypress e2e")
                tipoPrueba = "e2e";
            else if (comando == "Cypress random")
                tipoPrueba = "Random";
            else if (comando == "Webdriver.io BDT")
                tipoPrueba = "BDT";
            else if (comando == "Monkey")
                tipoPrueba = "RandomTesting";
            else if (comando == "Calabash BDT")
                tipoPrueba = "Calabash";
            else if (comando == "Mutation mobile monkey" || comando == "Mutation mobile calabash")
                tipoPrueba = "Mutation";
            else
            {
                tipoPrueba = "other";
            }

            string nombreArchivo = String.Join("/", new string[] { app.nombreDir, tipoPrueba
                , app.versionActiva, DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss"),  "ejecucionprueba.html" });
            nombreArchivo = MainWindow.DEFAULT_PATH + MainWindow.DEFAULT_DIRECTORY
                + Aplication.APLICATIONS_DIR + APP_SAVES_DIR + nombreArchivo;
            if (!Directory.Exists(System.IO.Path.GetDirectoryName(nombreArchivo)))
            {
                Directory.CreateDirectory(System.IO.Path.GetDirectoryName(nombreArchivo));
            }           

            mensajeBlock.Text = "Información guardada en: \n" + System.IO.Path.GetDirectoryName(nombreArchivo);

            //Copiar imágenes.
            string carpetaScreenshots = System.IO.Path.GetDirectoryName(nombreArchivo) + "/screenshots/";
            if (comando == "Cypress e2e")     {
                MoveFolder(MainWindow.DEFAULT_PATH + MainWindow.DEFAULT_DIRECTORY
                    + Aplication.APLICATIONS_DIR + "CypressFiles/cypress/screenshots/", carpetaScreenshots);
            }
            else if (comando == "Webdriver.io BDT")
            {
                if (Directory.Exists(MainWindow.DEFAULT_PATH + MainWindow.DEFAULT_DIRECTORY
                    + Aplication.APLICATIONS_DIR + "BDT/screenshots"))
                {
                    MoveFolder(MainWindow.DEFAULT_PATH + MainWindow.DEFAULT_DIRECTORY
                        + Aplication.APLICATIONS_DIR + "BDT/screenshots", carpetaScreenshots);
                }
            } else if (comando == "Monkey") {
                MoveFolder(MainWindow.DEFAULT_PATH + MainWindow.DEFAULT_DIRECTORY
                    + Aplication.APLICATIONS_DIR + "RandomTesting/" + app.nombre, carpetaScreenshots);
            } else if (comando == "Calabash BDT") {
                MoveFolder(MainWindow.DEFAULT_PATH + MainWindow.DEFAULT_DIRECTORY
                    + Aplication.APLICATIONS_DIR + "CalabashFiles/" + app.nombre, carpetaScreenshots);
            } else {
                tipoPrueba = "other";
            }

            string mypath = System.IO.Path.GetDirectoryName(nombreArchivo);
            string[] filePaths = Directory.GetFiles(carpetaScreenshots, "*.png", SearchOption.AllDirectories);
            cuerpoHtml.AppendLine("<div class=\"d-flex flex-row flex-nowrap justify-content-center\">");
            for (int i = 0; i < filePaths.Length; i++)
            {
                cuerpoHtml.AppendLine("<div class=\"card\" style=\"width: 18rem;\"><img class=\"card-img-top\" src=\"" + filePaths[i] + "\"></div>");
            }
            cuerpoHtml.AppendLine("</div>");
            escribirReporteHTML(nombreArchivo);

            ejecutarComando("start " + mypath
                , "");
        }

        private void MoveFolder(string source, string destination)
        {
            //move if directories are on the same volume
            if (System.IO.Path.GetPathRoot(source) == System.IO.Path.GetPathRoot(destination))
            {
                Directory.Move(source, destination);
            }
            else
            {
                DirectoryInfo diSource = new DirectoryInfo(source);
                DirectoryInfo diTarget = new DirectoryInfo(destination);
                CopyFilesRecursively(diSource, diTarget);
                Directory.Delete(source, true);
            }
        }

        private void CopyFilesRecursively(DirectoryInfo source, DirectoryInfo target)
        {
            foreach (DirectoryInfo dir in source.GetDirectories())
                CopyFilesRecursively(dir, target.CreateSubdirectory(dir.Name));
            foreach (FileInfo file in source.GetFiles())
                file.CopyTo(System.IO.Path.Combine(target.FullName, file.Name));
        }

        /// <summary>
        /// Agrega un elemento al reporte html de tipo elem con contenido content.
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="content"></param>
        private void agregarElementoReporteHTML(string elem, string content)
        {
            if(cuerpoHtml is null)
            {
                cuerpoHtml = new StringBuilder();
            }
            if (elem.Contains("h") && elem != "h5") elem = elem + " class=\"text-success\"";
            cuerpoHtml.AppendLine("<" + elem + ">" + content + "</" + elem + ">");
        }

        /// <summary>
        /// Escribe el reporte html.
        /// </summary>
        /// <param name="pathReporte"></param>
        private void escribirReporteHTML(string pathReporte)
        {
            using (StreamWriter sw = File.AppendText(pathReporte))
            {
                sw.WriteLine("<!DOCTYPE html>" + "\n"
                    + "<html>" + "\n"
                    + "    <head>" + "\n"
                    + "        <title> Reporte de pruebas </title>" + "\n"
                    + "        <meta charset=\"UTF - 8\">" + "\n"
                    + "         <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity =\"sha384 -MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin =\"anonymous\">"
                    + "    </head>" + "\n"
                    + "    <body> " + "\n"
                    + "     <div class=\"text-center\"> " + "\n"
                    + "        <h1 class=\"text-success\"> Reporte </h1> ");
                sw.WriteLine(cuerpoHtml.ToString());
                sw.WriteLine("</div>	</body>" + "\n"
                    + "</html>");
            }
        }

        public String ejecutarComando(String comando, String dir)
        {
            string path = dir.Equals("") ? "" : "cd /d " + dir + "&";

            ProcessStartInfo processStartInfo = new ProcessStartInfo("cmd", "/c " + path + comando);
            processStartInfo.RedirectStandardOutput = true;
            processStartInfo.RedirectStandardError = true;
            processStartInfo.WindowStyle = ProcessWindowStyle.Maximized;  //No tiene efecto tal vez por ser la cmd
            processStartInfo.CreateNoWindow = true; //Ocultar consola de comandos
            processStartInfo.UseShellExecute = false;

            Process process = new Process();
            StringBuilder processOutput = new StringBuilder("");
            StringBuilder processErrorOutput = new StringBuilder("");
            process.OutputDataReceived += new DataReceivedEventHandler(
                (sendingProcess, outLine) =>
                {
                    // Collect the sort command output.
                    if (!String.IsNullOrEmpty(outLine.Data))
                    {
                        // Add the text to the collected output.
                        processOutput.AppendLine(outLine.Data);
                        Console.WriteLine(outLine.Data);
                    }
                }
            );
            process.ErrorDataReceived += new DataReceivedEventHandler(
                (sendingProcess, outLine) =>
                {
                    // Collect the sort command output.
                    if (!String.IsNullOrEmpty(outLine.Data))
                    {
                        // Add the text to the collected output.
                        processErrorOutput.AppendLine(outLine.Data);
                        Console.WriteLine(outLine.Data);
                    }
                }
            );
            process.StartInfo = processStartInfo;
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();
            process.Dispose();

            return processOutput.ToString() + " " + processErrorOutput.ToString();
        }

    }




}
