﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Testing.Testing_config_data_types;

namespace Testing
{
    /// <summary>
    /// Lógica de interacción para RegistroApp.xaml
    /// </summary>
    public partial class Config : Window
    {
        private Aplication app;
        private MainWindow mainWindow;

        public Config()
        {
            InitializeComponent();
        }

        public Config(Aplication appParam, MainWindow mainWindow) : this()
        {
            this.app = appParam;
            this.mainWindow = mainWindow;
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Muestra el panel de configuración de la prueba seleccionada.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tipo_ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (tipoComboBox.SelectedIndex)
            {
                case 0: //E2E
                    E2EPane.Visibility = Visibility.Visible;
                    MonkeyPane.Visibility = Visibility.Collapsed;
                    BDTPane.Visibility = Visibility.Collapsed;
                    VersionPane.Visibility = Visibility.Collapsed;
                    mutationPane.Visibility = Visibility.Collapsed;
                    break;
                case 1: //Monkey
                    E2EPane.Visibility = Visibility.Collapsed;
                    MonkeyPane.Visibility = Visibility.Visible;
                    BDTPane.Visibility = Visibility.Collapsed;
                    VersionPane.Visibility = Visibility.Collapsed;
                    mutationPane.Visibility = Visibility.Collapsed;
                    break;
                case 2: //BDT
                    E2EPane.Visibility = Visibility.Collapsed;
                    MonkeyPane.Visibility = Visibility.Collapsed;
                    BDTPane.Visibility = Visibility.Visible;
                    VersionPane .Visibility = Visibility.Collapsed;
                    mutationPane.Visibility = Visibility.Collapsed;
                    break;
                case 3: //Version
                    E2EPane.Visibility = Visibility.Collapsed;
                    MonkeyPane.Visibility = Visibility.Collapsed;
                    BDTPane.Visibility = Visibility.Collapsed;
                    VersionPane.Visibility = Visibility.Visible;
                    mutationPane.Visibility = Visibility.Collapsed;
                    cargarVersiones();
                    break;
                case 4: //Mutation
                    E2EPane.Visibility = Visibility.Collapsed;
                    MonkeyPane.Visibility = Visibility.Collapsed;
                    BDTPane.Visibility = Visibility.Collapsed;
                    VersionPane.Visibility = Visibility.Collapsed;
                    mutationPane.Visibility = Visibility.Visible;
                    break;
               
            }
        }

        /// <summary>
        /// Configura la prueba de la aplicación. Se llama al presionar el botón "Finalizar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void configApp_Click(object sender, RoutedEventArgs e)
        {
            int select = tipoComboBox.SelectedIndex;

            switch (tipoComboBox.SelectedIndex)
            {
                case 0: //E2E
                    app.e2EDataType = new e2EDataType
                    {
                        herramienta = herramientasComboBox.Text,
                        navegador = navegadorComboBox.Text,
                        resolucion = resoluciónComboBox.Text,
                        isHeadless = isHeadless.IsChecked,
                        archivoPruebas = fileLocationCypress.Text
                    };
                    break;
                case 1: //Monkey
                    app.monkeyDataType = new MonkeyDataType
                    {
                        herramienta = herramientasComboBoxMonkey.Text,
                        archivoPruebas = fileLocationSDK.Text,
                        eventos = Int32.Parse(numEventos.Text)
                    };
                    break;
                case 2: //BDT
                    app.bDTDataType = new BDTDataType
                    {
                        herramienta = herramientasComboBoxBDT.Text,
                        ruta_apk = apkLocationBDT.Text
                    };
                    break;
                case 3: //Version
                    break;
                case 4: //Mutation
                    app.MDataType = new MutationDataType
                    {
                        numeroMutantes = Int32.Parse(numberMutants.Text),
                        nombrePaquete = mutantsPackageName.Text,
                        directorioAMutar = mutantsSourceDirectoryLocation.Text,
                        programaParaFirmaApk = apkSignerProgram.Text
                    };
                    break;
                default:
                    MessageBox.Show("Seleccione un tipo de prueba.");
                    break;
            }

            app.GuardarApp();
            this.Close();
        }

        private void herramientasComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void navegadorComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void resoluciónComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        /// <summary>
        /// Define la ubicación del archivo de pruebas E2E de Cypress.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CambiarButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            //openFileDialog.InitialDirectory = Aplication.PATH;
            if (openFileDialog.ShowDialog() == true)
                fileLocationCypress.Text = openFileDialog.FileName;
        }

        private void CambiarDirButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog fdb = new System.Windows.Forms.FolderBrowserDialog();
            fdb.RootFolder = Environment.SpecialFolder.MyComputer;
            fdb.Description = "Selecione una ruta del SDK";
            fdb.ShowNewFolderButton = false;

            if (fdb.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                fileLocationSDK.Text = fdb.SelectedPath;
            }
        }

        /// <summary>
        /// Define la ubicación del archivo de features BDT
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CambiarButtonBDT_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            //openFileDialog.InitialDirectory = Aplication.PATH;
          //  if (openFileDialog.ShowDialog() == true)
                //fileLocationBDT.Text = openFileDialog.FileName;
        }



        /// <summary>
        /// Cambia la versión activa de la aplicación.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void activarVersion(object sender, RoutedEventArgs e)
        {
            app.versionActiva = (string)activarVersionComboBox.SelectedValue;
            activeVersionTextBox.Text = app.versionActiva;
        }

        /// <summary>
        /// Agrega una versión a la aplicación.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void agregarVersion(object sender, RoutedEventArgs e)
        {
            app.versiones.Add(newVersionTextBox.Text);
            cargarVersiones();
        }

        /// <summary>
        /// Carga la lista de versiones disponibles de la aplicación.
        /// </summary>
        public void cargarVersiones()
        {
            activarVersionComboBox.Items.Clear();
            foreach (string ver in app.versiones)
            {
                activarVersionComboBox.Items.Add(ver);
            }
            activeVersionTextBox.Text = app.versionActiva;
        }

        /// <summary>
        /// Cambia la ubicación del directorio del proyecto.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cambiarDirectorioProyecto_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog fdb = new System.Windows.Forms.FolderBrowserDialog();
            fdb.RootFolder = Environment.SpecialFolder.MyComputer;
            fdb.Description = "Selecione la ubicación del proyecto";
            fdb.ShowNewFolderButton = false;

            if (fdb.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                programDirectoryLocation.Text = fdb.SelectedPath;
            }
        }

        /// <summary>
        /// Cambia la ubicación del directorio con las fuentes a mutar.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cambiarDirectorioFuenteMutantes_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog fdb = new System.Windows.Forms.FolderBrowserDialog();
            fdb.RootFolder = Environment.SpecialFolder.MyComputer;
            fdb.Description = "Selecione la ubicación de las fuentes a mutar";
            fdb.ShowNewFolderButton = false;

            if (fdb.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                mutantsSourceDirectoryLocation.Text = fdb.SelectedPath;
            }
        }

        /// <summary>
        /// Cambia la ubicación del directorio con el apk generado.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cambiarDirectorioApkGenerada_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog fdb = new System.Windows.Forms.FolderBrowserDialog();
            fdb.RootFolder = Environment.SpecialFolder.MyComputer;
            fdb.Description = "Selecione la ubicación de la apk generada";
            fdb.ShowNewFolderButton = false;

            if (fdb.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                apkDirectoryLocation.Text = fdb.SelectedPath;
            }
        }

        /// <summary>
        /// Genera los mutantes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void generarMutantes_Click(object sender, RoutedEventArgs e)
        {
            //Generar fuentes mutadas.
            string comando = @"java -jar .\MDroidPlus-1.0.0.jar .\combined.jar"
                 + " " + mutantsSourceDirectoryLocation.Text
                 + " " + mutantsPackageName.Text
                 + " " + MainWindow.DEFAULT_PATH + MainWindow.DEFAULT_DIRECTORY + Aplication.APLICATIONS_DIR + @"mdroidplus-Mutation\mutants-source\"
                 + " " + MainWindow.DEFAULT_PATH + MainWindow.DEFAULT_DIRECTORY + Aplication.APLICATIONS_DIR + @"mdroidplus-Mutation\"
                 + " false";
            string directorio = MainWindow.DEFAULT_PATH + MainWindow.DEFAULT_DIRECTORY + Aplication.APLICATIONS_DIR + @"mdroidplus-Mutation\";
            this.mainWindow.ejecutarComando(comando, directorio);

            //Generar APK.
            foreach (string SourcePath in Directory.GetDirectories(MainWindow.DEFAULT_PATH + MainWindow.DEFAULT_DIRECTORY 
                + Aplication.APLICATIONS_DIR + @"mdroidplus-Mutation\mutants-source\"))
            {
                //Now Create all of the directories.
                foreach (string dirPath in Directory.GetDirectories(SourcePath, "*",
                    SearchOption.AllDirectories))
                    Directory.CreateDirectory(dirPath.Replace(SourcePath, mutantsSourceDirectoryLocation.Text));

                //Copy all the files & Replaces any files with the same name.
                foreach (string newPath in Directory.GetFiles(SourcePath, "*.*",
                    SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(SourcePath, mutantsSourceDirectoryLocation.Text), true);

                //Build APK. 
                this.mainWindow.ejecutarComando(mutantsCompileCommand.Text, programDirectoryLocation.Text);

                //Copy APK.
                string apkName = System.IO.Path.GetFileName(SourcePath).Replace(mutantsPackageName.Text + "-mutant", "");
                string[] genApk = Directory.GetFiles(apkDirectoryLocation.Text, "*.apk",
                    SearchOption.AllDirectories);
                File.Copy(genApk[0], MainWindow.DEFAULT_PATH + MainWindow.DEFAULT_DIRECTORY
                + Aplication.APLICATIONS_DIR + @"mdroidplus-Mutation\mutants\" + apkName + ".apk");
            }
        }

        /// <summary>
        /// Cambia el programa para firmar las apk.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cambiarProgramaFirmaApk_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                apkSignerProgram.Text = openFileDialog.FileName;
        }
    }


}
