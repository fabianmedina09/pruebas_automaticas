﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Testing
{
    /// <summary>
    /// Lógica de interacción para RegistroApp.xaml
    /// </summary>
    public partial class GenData : Window
    {

        private string urlAPI;
        private string nombre;

        public GenData()
        {
            InitializeComponent();
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();            
        }

        private void genData_Click(object sender, RoutedEventArgs e)
        {
            if (this.urlAPI != null && items.Text.Length > 0) {
                string response = "";
                try
                {
                    RestClient rclient = new RestClient();
                    rclient.endPoint = this.urlAPI + "?key=2b63a040&items=" +items.Text; 
                    response = string.Empty;
                    response = rclient.makeRequest();
                    debugOutput(response);
                } catch {
                    msj.Text = "Error al consumir API";
                    msj.Foreground = Brushes.Red;
                    resultado.Text = response;
                    resultado.Foreground = Brushes.Red;
                }
            } else {
                msj.Text = "Datos inválidos";
                msj.Foreground = Brushes.Red;
            }
        }

        private void descargar_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void regresar_Click(object sender, RoutedEventArgs e)
        {
            //MainWindow mw = new MainWindow();
            //mw.Show();
            this.Close();
        }

        private void debugOutput(string debugText) {
            try {
                crearDirectorio(MainWindow.DEFAULT_PATH + MainWindow.DEFAULT_DIRECTORY + "/Projects/MockarooData");
                //System.Diagnostics.Debug.Write(debugText + Environment.NewLine);
                using (StreamWriter sw = File.CreateText(MainWindow.DEFAULT_PATH + MainWindow.DEFAULT_DIRECTORY + "/Projects/MockarooData/" + this.nombre + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + ".json"))
                {
                    sw.WriteLine(debugText + Environment.NewLine);
                }
                msj.Text = "Se han generado datos en: ";
                msj.Foreground = Brushes.Green;
                resultado.Text = "../Projects/MockarooData/" + this.nombre + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + ".json";
                resultado.Foreground = Brushes.Green;
            } catch {

            }

        }

        private void tipo_ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (appComboBox.SelectedIndex)
            {
                case 0: //LimeSurvey
                    this.urlAPI = "https://my.api.mockaroo.com/limesurvey/surveys.json";
                    this.nombre = "LimeSurvey";
                    
                    break;
                case 1: //OmniNotes
                    this.urlAPI = "https://my.api.mockaroo.com/omninotes/notes.json";
                    this.nombre = "OmniNotes";
                    break;
            }

        }

        private void crearDirectorio(String path)
        {
            if (!Directory.Exists(System.IO.Path.Combine(path)))
            {
                Directory.CreateDirectory(System.IO.Path.Combine(path));
            }
        }

    }
}
